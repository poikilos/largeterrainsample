#include "BaseApplication.h"

#define ENDLESS_PAGING

// max range for a int16
#define ENDLESS_PAGE_MIN_X (-0x7FFF)
#define ENDLESS_PAGE_MIN_Y (-0x7FFF)
#define ENDLESS_PAGE_MAX_X 0x7FFF
#define ENDLESS_PAGE_MAX_Y 0x7FFF

#include "BaseApplication.h"
#include "OIS/OIS.h"
#include "OGRE/Overlay/OgreOverlay.h"
#include "OGRE/Overlay/OgreOverlayManager.h"
#include "OGRE/Overlay/OgreOverlayElement.h"
#include "OGRE/ExampleApplication.h"
#include "OGRE/Terrain/OgreTerrain.h"
#include "OGRE/Terrain/OgreTerrainGroup.h"
#include "OGRE/Terrain/OgreTerrainQuadTreeNode.h"
#include "OGRE/Terrain/OgreTerrainMaterialGeneratorA.h"
#include "OGRE/Terrain/OgreTerrainPagedWorldSection.h"
#include "OGRE/Terrain/OgreTerrainPaging.h"

#include "PerlinNoiseTerrainGenerator.h"

#define ENDLESS_TERRAIN_FILE_PREFIX String("EndlessWorldTerrain")
#define ENDLESS_TERRAIN_FILE_SUFFIX String("dat")
#define TERRAIN_WORLD_SIZE 12000.0f
#define TERRAIN_SIZE 513
#define HOLD_LOD_DISTANCE 3000.0

using namespace Ogre;

class LargeTerrainApplication : public BaseApplication
{
public:
    LargeTerrainApplication(void) : mTerrainGroup(0)
		, mTerrainPaging(0)
		, mPageManager(0)
		, mPagedWorld(0)
		, mTerrainPagedWorldSection(0)
		, mPerlinNoiseTerrainGenerator(0)
        , mLodStatus(false)
		, mAutoLod(true)
		, mFly(true)
		, mFallVelocity(0)
        , mTerrainPos(0,0,0)
    {

    }

    virtual ~LargeTerrainApplication(void)
    {
		if(mTerrainPaging)
		{
			OGRE_DELETE mTerrainPaging;
			mPageManager->destroyWorld( mPagedWorld );
			OGRE_DELETE mPageManager;
		}

		OGRE_DELETE mTerrainGlobals;
    }

protected:
	TerrainGlobalOptions* mTerrainGlobals;
	TerrainGroup* mTerrainGroup;
	TerrainPaging* mTerrainPaging;
	PageManager* mPageManager;
	PagedWorld* mPagedWorld;
	TerrainPagedWorldSection* mTerrainPagedWorldSection;
	PerlinNoiseTerrainGenerator* mPerlinNoiseTerrainGenerator;
	bool mLodStatus;
	bool mAutoLod;

	class DummyPageProvider : public PageProvider
	{
	public:
		bool prepareProceduralPage(Page* page, PagedWorldSection* section) { return true; }
		bool loadProceduralPage(Page* page, PagedWorldSection* section) { return true; }
		bool unloadProceduralPage(Page* page, PagedWorldSection* section) { return true; }
		bool unprepareProceduralPage(Page* page, PagedWorldSection* section) { return true; }
	};

	DummyPageProvider mDummyPageProvider;

	bool mFly;
	Real mFallVelocity;
	Vector3 mTerrainPos;

    virtual void createScene(void)
    {
    	mTerrainGlobals = OGRE_NEW TerrainGlobalOptions();
		mCameraMan->setTopSpeed(100);

		MaterialManager::getSingleton().setDefaultTextureFiltering(TFO_ANISOTROPIC);
		MaterialManager::getSingleton().setDefaultAnisotropy(7);

		mSceneMgr->setFog(FOG_LINEAR, ColourValue(0.7, 0.7, 0.8), 0, 4000, 10000);

		LogManager::getSingleton().setLogDetail(LL_BOREME);

		Vector3 lightdir(0.55, -0.3, 0.75);
		lightdir.normalise();

		Light* l = mSceneMgr->createLight("tstLight");
		l->setType(Light::LT_DIRECTIONAL);
		l->setDirection(lightdir);
		l->setDiffuseColour(ColourValue::White);
		l->setSpecularColour(ColourValue(0.4, 0.4, 0.4));

		mSceneMgr->setAmbientLight(ColourValue(0.2, 0.2, 0.2));

		mTerrainGroup = OGRE_NEW TerrainGroup(mSceneMgr, Terrain::ALIGN_X_Z, TERRAIN_SIZE, TERRAIN_WORLD_SIZE);
		mTerrainGroup->setFilenameConvention(ENDLESS_TERRAIN_FILE_PREFIX, ENDLESS_TERRAIN_FILE_SUFFIX);
		mTerrainGroup->setOrigin(mTerrainPos);
		mTerrainGroup->setAutoUpdateLod( TerrainAutoUpdateLodFactory::getAutoUpdateLod(BY_DISTANCE) );

		configureTerrainDefaults(l);

		// Paging setup
		mPageManager = OGRE_NEW PageManager();
		// Since we're not loading any pages from .page files, we need a way just 
		// to say we've loaded them without them actually being loaded
		mPageManager->setPageProvider(&mDummyPageProvider);
		mPageManager->addCamera(mCamera);
		mPageManager->setDebugDisplayLevel(0);
		mTerrainPaging = OGRE_NEW TerrainPaging(mPageManager);
		mPagedWorld = mPageManager->createWorld();
		mTerrainPagedWorldSection = mTerrainPaging->createWorldSection(mPagedWorld, mTerrainGroup, 400, 500, 
			ENDLESS_PAGE_MIN_X, ENDLESS_PAGE_MIN_Y, 
			ENDLESS_PAGE_MAX_X, ENDLESS_PAGE_MAX_Y);

		mPerlinNoiseTerrainGenerator = OGRE_NEW PerlinNoiseTerrainGenerator;
		mTerrainPagedWorldSection->setDefiner( mPerlinNoiseTerrainGenerator );
//		mTerrainPagedWorldSection->setDefiner( OGRE_NEW SimpleTerrainDefiner );

		mTerrainGroup->freeTemporaryResources();

		mSceneMgr->setSkyBox(true, "Examples/CloudyNoonSkyBox");
    }

    bool frameRenderingQueued(const FrameEvent& evt)
    {
		if (!mFly)
		{
			// clamp to terrain
			Vector3 camPos = mCamera->getPosition();
			Ray ray;
			ray.setOrigin(Vector3(camPos.x, mTerrainPos.y + 10000, camPos.z));
			ray.setDirection(Vector3::NEGATIVE_UNIT_Y);

			TerrainGroup::RayResult rayResult = mTerrainGroup->rayIntersects(ray);
			const Real distanceAboveTerrain = 50;
			if (rayResult.hit)
				mCamera->setPosition(camPos.x, rayResult.position.y + distanceAboveTerrain, camPos.z);
		}

		if (mTerrainGroup->isDerivedDataUpdateInProgress())
		{
			// Building terrain...
		}
		else
		{
			// Terrain building finished!
		}

		if (mLodStatus)
		{
			TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();
			while(ti.hasMoreElements())
			{
				Terrain* t = ti.getNext()->instance;
				if (!t)
					continue;

				Vector3 pt = mCamera->getProjectionMatrix() * (mCamera->getViewMatrix() * t->getPosition());
				Real x = (pt.x / 2) + 0.5f;
				Real y = 1 - ((pt.y / 2) + 0.5f);
			}
		}

		mTerrainGroup->autoUpdateLodAll(false, Any( Real(HOLD_LOD_DISTANCE) ));
        return BaseApplication::frameRenderingQueued(evt);  // don't forget the parent updates!
    }

	bool keyPressed (const OIS::KeyEvent &e)
	{
		switch (e.key)
		{
		case OIS::KC_PGUP:
			{
                TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();
				while(ti.hasMoreElements())
				{
					Terrain* t = ti.getNext()->instance;
					if (t)
						t->increaseLodLevel();
				}
			}
			break;
		case OIS::KC_PGDOWN:
			{
				TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();
				while(ti.hasMoreElements())
				{
					Terrain* t = ti.getNext()->instance;
					if (t)
						t->decreaseLodLevel();
				}
			}
			break;
		// generate new random offset, to make terrains different
		case OIS::KC_G:
			if(mPerlinNoiseTerrainGenerator)
			{
				// random a new origin point
				mPerlinNoiseTerrainGenerator->randomize();

				// reload all terrains
				TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();
				while(ti.hasMoreElements())
				{
					TerrainGroup::TerrainSlot* slot = ti.getNext();
					PageID pageID = mTerrainGroup->packIndex( slot->x, slot->y );
					mTerrainPagedWorldSection->unloadPage(pageID);
					mTerrainPagedWorldSection->loadPage(pageID);
				}
			}
			break;
		default:
            return BaseApplication::keyPressed(e);
		}

		return true;
	}

	void configureTerrainDefaults(Light* l)
	{
		// Configure global
		mTerrainGlobals->setMaxPixelError(8);
		// testing composite map
		mTerrainGlobals->setCompositeMapDistance(3000);
		//mTerrainGlobals->setUseRayBoxDistanceCalculation(true);
		mTerrainGlobals->getDefaultMaterialGenerator()->setLightmapEnabled(false);

		mTerrainGlobals->setCompositeMapAmbient(mSceneMgr->getAmbientLight());
		//mTerrainGlobals->setCompositeMapAmbient(ColourValue::Red);
		mTerrainGlobals->setCompositeMapDiffuse(l->getDiffuseColour());

		// Configure default import settings for if we use imported image
		Terrain::ImportData& defaultimp = mTerrainGroup->getDefaultImportSettings();
		defaultimp.terrainSize = TERRAIN_SIZE;
		defaultimp.worldSize = TERRAIN_WORLD_SIZE;
		defaultimp.inputScale = 600;
		defaultimp.minBatchSize = 33;
		defaultimp.maxBatchSize = 65;
		// textures
		defaultimp.layerList.resize(3);
		defaultimp.layerList[0].worldSize = 100;
		defaultimp.layerList[0].textureNames.push_back("dirt_grayrocky_diffusespecular.dds");
		defaultimp.layerList[0].textureNames.push_back("dirt_grayrocky_normalheight.dds");
		defaultimp.layerList[1].worldSize = 30;
		defaultimp.layerList[1].textureNames.push_back("grass_green-01_diffusespecular.dds");
		defaultimp.layerList[1].textureNames.push_back("grass_green-01_normalheight.dds");
		defaultimp.layerList[2].worldSize = 200;
		defaultimp.layerList[2].textureNames.push_back("growth_weirdfungus-03_diffusespecular.dds");
		defaultimp.layerList[2].textureNames.push_back("growth_weirdfungus-03_normalheight.dds");
	}

	/*-----------------------------------------------------------------------------
	| Extends setupView to change some initial camera settings for this sample.
	-----------------------------------------------------------------------------*/
	void setupView()
	{
		// put camera at world center, so that it's difficult to reach the edge
		Vector3 worldCenter(
			(ENDLESS_PAGE_MAX_X+ENDLESS_PAGE_MIN_X) / 2 * TERRAIN_WORLD_SIZE,
			0,
			-(ENDLESS_PAGE_MAX_Y+ENDLESS_PAGE_MIN_Y) / 2 * TERRAIN_WORLD_SIZE
			);
		mCamera->setPosition(mTerrainPos+worldCenter);
		mCamera->lookAt(mTerrainPos);
		mCamera->setNearClipDistance(0.1);
		mCamera->setFarClipDistance(50000);

		if (mRoot->getRenderSystem()->getCapabilities()->hasCapability(RSC_INFINITE_FAR_PLANE))
        {
            mCamera->setFarClipDistance(0);   // enable infinite far clip distance if we can
        }
	}

	class SimpleTerrainDefiner : public TerrainPagedWorldSection::TerrainDefiner
	{
	public:
		virtual void define(TerrainGroup* terrainGroup, long x, long y)
		{
			Image img;
			img.load("terrain.png", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
			if (x % 2)
				img.flipAroundY();
			if (y % 2)
				img.flipAroundX();
			terrainGroup->defineTerrain(x, y, &img);
		}
	};
};

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        LargeTerrainApplication app;

        try {
            app.go();
        } catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occured: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif
